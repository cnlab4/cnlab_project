var http = require('http'),
    fs = require('fs'),
    path = require('path'),
    url = require("url");

var data,callback,chunk,server,pathname,filePath;
var args = process.argv.slice(2);
var options = {
  host: args[0],
  path: '/',
  port: '5000',
  method: 'POST'
};


var server = http.createServer(function(request,response){
    response.writeHead(200, {"Content-Type": "text/plain;charset=utf-8" });
    var pathname = url.parse(request.url).pathname;
    //var writeStream = fs.createWriteStream('./out.txt');
    pathname = pathname;
    console.log(pathname);
    filePath = pathname.toString();
    if(filePath != '/')
    {
        fs.exists(filePath,(callback) => {
            if(callback == false)
            {
                response.end('file not exists');
            }
            else
            {
                var req = http.request(options, function(res) {
                    res.on('data',(chunk) =>{
                        //writeStream.write(chunk);
                        response.write(chunk.toString());
												console.log(chunk.toString());
                    });
                    res.on('end',() =>{
                        console.log('end');   
                        response.end(); 
                    });
                });
                //response.write('wait\n');
                fs.readFile(filePath,(err,data) => {
                    req.write(data);
                    req.end();
                });

            }
        });
        
    }
})
.listen(5100);



