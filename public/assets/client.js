// grab the room from the URL
var room = location.search && location.search.split('?')[1];
var userName = document.getElementById("username").innerHTML;

alert("Welcome to Vid.com!");

// create our webrtc connection
var webrtc = new SimpleWebRTC({
	// the id/element dom element that will hold "our" video
	localVideoEl: 'local-video',
	// the id/element dom element that will hold remote videos
	remoteVideosEl: '',
	// immediately ask for camera access
	autoRequestMedia: true,
	debug: false,
	nick: userName,
	detectSpeakingEvents: true,
	autoAdjustMic: false
});

// when it's ready, join if we got a room from the URL
webrtc.on('readyToCall', function () {
	// you can name it anything
	if (room) webrtc.joinRoom(room);
});

function showVolume(el, volume) {
	var level;
	if (!el) return;
	if (volume < -80) { // vary between -45 and -20
		level = el.children.length;
	} 
	else if (volume > -10) {
		level = 0; 
	} 
	else {
		level = Math.floor((volume + 80) / 70 * el.children.length);
	}
	// reset
	var i;
	for(i = 0; i < level; i++){
		el.children[i].className = el.children[i].className.replace( /(?:^|\s)active(?!\S)/g , '' );
	}
	// set active
	for(i = level; i < el.children.length; i++){
		el.children[i].className = el.children[i].className.replace( /(?:^|\s)active(?!\S)/g , '' );
		el.children[i].className += " active";
	}
}
webrtc.on('channelMessage', function (peer, label, data) {
	if (data.type == 'volume') {
		showVolume(document.getElementById('volume_' + peer.id), data.volume);
	}
});
webrtc.on('videoAdded', function (video, peer) {
	console.log('video added', peer);
	var remotes = document.getElementById('remotes');
	if (remotes) {
		var d = document.createElement('div');
		d.className = 'user-video-box';
		d.id = 'container_' + webrtc.getDomId(peer);
		d.appendChild(video);
		var mic = document.createElement('div');
		mic.className = 'mic-volume-remote';
		var vol = document.createElement('div');
		vol.id = 'volume_' + peer.id;
		vol.className = 'volume-meter volume-meter-vertical-remote';
		var i;
		for(i = 0; i < 10; i++){
			var tmp = document.createElement('div');
			tmp.className = 'volume-meter-segment-remote';
			vol.appendChild(tmp);
		}
		mic.appendChild(vol);
		var icon = document.createElement('i');
		icon.className = "fa fa-volume-up";
		icon.style = "color:white; font-size: 2em;";
		mic.appendChild(icon);
		video.onclick = function () {
			video.style.width = video.videoWidth + 'px';
			video.style.height = video.videoHeight + 'px';
		};
		d.appendChild(mic);
		remotes.appendChild(d);
	}
});
webrtc.on('videoRemoved', function (video, peer) {
	console.log('video removed ', peer);
	var remotes = document.getElementById('remotes');
	var el = document.getElementById('container_' + webrtc.getDomId(peer));
	if (remotes && el) {
		remotes.removeChild(el);
	}
});
webrtc.on('volumeChange', function (volume, treshold) {
	//console.log('own volume', volume);
	showVolume(document.getElementById('local-volume'), volume);
});

// send message
webrtc.connection.on('message', function(data){
	if(data.type === 'chat'){
		console.log('chat received',data);
		var name = data.payload.nick;
		var msg = data.payload.message;
		var date = data.payload.date;
		$('#messages').append("<li class='left clearfix'><span class='chat-img pull-left'> <img src=\'http://placehold.it/50/55C1E7/fff&text=" + name.charAt(0).toUpperCase() + "\'alt='User Avatar' class='img-circle' /> </span> <div class='chat-body clearfix'> <div class='header'> <strong class='primary-font'>"+ name +"</strong> <small class='pull-right text-muted'> <span class='glyphicon glyphicon-time'></span>"+ date +"</small> </div> <p>"+ msg +"</p> </div> </li>");
		var divx = document.getElementById("panel-body-id");
		divx.scrollTop = divx.scrollHeight;
	}
});
// if user clicks chat-button or presses enter key, send message to peers
$('#btn-chat').click(chat_handler);
$('#input-chat').keypress(function(event){
	if(event.keyCode == 13){
		$('#btn-chat').click();
		return false;
	}
});
function chat_handler(){
	var msg = $('#input-chat').val();
	var date = new Date();
	date = date.toLocaleTimeString();	webrtc.sendToAll('chat', {message: msg, nick: webrtc.config.nick, date: date});
	$('#messages').append("<li class='right clearfix'><span class='chat-img pull-right'><img src='http://placehold.it/50/FA6F57/fff&text=Me' alt='User Avatar' class='img-circle' /></span><div class='chat-body clearfix'> <div class='header'> <small class='text-muted'><span class='glyphicon glyphicon-time'></span>" + date + "</small> <strong class='pull-right primary-font'>"+ webrtc.config.nick +"</strong> </div> <p>" + msg +"</p></div></li>");
	$('#input-chat').val('');
	var divx = document.getElementById("panel-body-id");
	divx.scrollTop = divx.scrollHeight;
}


// share screen
$("#btn-share").click(function () {
	if (webrtc.getLocalScreen()) {
		webrtc.stopScreenShare();
		$("#btn-share").removeClass("active");
		$("#btn-share").css("background", "white");
	} else {
		webrtc.shareScreen(function (err) {
			if (err) {
				$("#btn-share").removeClass("active");
				$("#btn-share").css("background", "white");
			} else {
				$("#btn-share").addClass("active");
				$("#btn-share").css("background", "red");
			}
		});

	}
});

//Mute local client
$("#btn-mute").click(function() {
	if( $("#btn-mute").hasClass("active")){
		webrtc.unmute();
		$("#btn-mute").removeClass("active");
		$("#btn-mute").css("background", "white");
	}
	else{
		webrtc.mute();
		$("#btn-mute").addClass("active");
		$("#btn-mute").css("background", "red");
	}
});

// Pause local client
$("#btn-pause").click(function() {
	if( $("#btn-pause").hasClass("active")){
		webrtc.resumeVideo();
		$("#btn-pause").removeClass("active");
		$("#btn-pause").css("background", "white")
	}
	else{
		webrtc.pauseVideo();
		$("#btn-pause").addClass("active");
		$("#btn-pause").css("background", "red")
	}
});
// Since we use this twice we put it here
function setRoom(name) {
	$('#createRoom').remove();
	$('h1').text('Video Conference')
	$('#subTitle').text('Link to join: ' + location.href);
	$('body').addClass('active');
}
if (room) {
	setRoom(room);
} else {
	$('#createRoom').submit(function () {
		webrtc.createRoom('', function (err, name) {
			console.log(' create room cb', arguments);

			var newUrl = location.pathname + '?' + name;
			if (!err) {
			history.replaceState({foo: 'bar'}, null, newUrl);
			setRoom(name);
		} else {
			console.log(err);
		}
	});
	return false;          
});
}


