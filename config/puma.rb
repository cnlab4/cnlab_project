workers 8
threads 1, 6

preload_app!

app_dir = File.expand_path("../..", __FILE__)
shared_dir = "#{app_dir}/shared"

# Default to production
environment ENV['RAILS_ENV'] || "production"

# Set up socket location
bind "unix://#{shared_dir}/sockets/puma.sock"

# Set up ssl key location
#ssl_bind '140.112.41.99', '9000', {
	#key: '/etc/letsencrypt/archive/xdn41o99.ee.ntu.edu.tw-0001/privkey1.pem',
	#cert: '/etc/letsencrypt/archive/xdn41o99.ee.ntu.edu.tw-0001/cert1.pem' 
#}

# Logging
stdout_redirect "#{shared_dir}/log/puma.stdout.log", "#{shared_dir}/log/puma.stderr.log", true

# Set master PID and state locations
pidfile "#{shared_dir}/pids/puma.pid"
state_path "#{shared_dir}/pids/puma.state"
activate_control_app


on_worker_boot do
	require "active_record"
	ActiveRecord::Base.connection.disconnect! rescue ActiveRecord::ConnectionNotEstablished
	ActiveRecord::Base.establish_connection
end
