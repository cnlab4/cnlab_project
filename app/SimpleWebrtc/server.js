// silly chrome wants SSL to do screensharing
var fs = require('fs'),
    express = require('express'),
    https = require('https'),
    http = require('http');


var privateKey = fs.readFileSync('fakekeys/key.pem').toString(),
    certificate = fs.readFileSync('fakekeys/cert.pem').toString();


var app = express();

app.use(express.static(__dirname));

https.createServer({key: privateKey, cert: certificate}, app).listen(4000);
http.createServer(app).listen(4001);

console.log('running on https://localhost:4000 and http://localhost:4001');
