class ApplicationMailer < ActionMailer::Base
  default from: "noreply@vid-conf.com"
  layout 'mailer'
end
