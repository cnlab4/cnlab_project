class StaticPagesController < ApplicationController
  def home
		if logged_in?
			@micropost = current_user.microposts.build
			@feed_items = current_user.feed
		else
			redirect_to root_url
		end
  end

  def help
  end

	def about
	end

	def contact
	end

	def index
		#render :layout => false
	end
end
