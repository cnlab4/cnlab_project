class RoomsController < ApplicationController
	before_action :logged_in_user
  
	def new
		@user = current_user
		render :layout => false
  end
end
